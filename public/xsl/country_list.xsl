<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="country_name" />
<xsl:param name="continent" />

<xsl:variable name="country_filter"
    select="countries/country[
                contains(name,$country_name)
                and contains(@continent,$continent)]"
  />

<xsl:template match="/">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Country</th>
        <th>Continent</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="$country_filter">
        <xsl:sort select="name" order="ascending"/>
          <tr>
            <td class="col-2">
              <img src="{flag/@src}"  class="img-fluid" alt="{flag/@src}"/>
            </td>
            <td class="col-7">
              <a class="text-dark" href="/html/country_infos.html?country={name}">
              <xsl:value-of select="name"/></a>
            </td>
            <td class="col-3">
              <xsl:value-of select="@continent"/>
            </td>
          </tr>
      </xsl:for-each>
    </tbody>
  </table>
</xsl:template>

</xsl:stylesheet>
