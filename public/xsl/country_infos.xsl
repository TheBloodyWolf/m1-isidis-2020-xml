<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 
<xsl:decimal-format name="d"
  decimal-separator="." grouping-separator=","/>
<xsl:param name="country_name"/>

<xsl:template match="/">
	<xsl:apply-templates select="/countries/country[name=$country_name]"/>
</xsl:template>

<xsl:template match="country">
	<h1 class="row">
		<xsl:value-of select="name"/>
		<span class="badge badge-light"><xsl:value-of select="name/@cc"/></span>
	</h1>
	<hr></hr>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<img src="{flag/@src}" class="img-fluid" alt="{flag/@src}"/>
		</div> 
		<div class="col-lg-6  col-md-6 col-sm-12">
			<p><b>Capital city : </b><xsl:value-of select="capital"/></p>
			<p><b>Population : </b><xsl:value-of select="format-number(population,'###,###','d')"/> inhabitants</p>
			<p><b>Area : </b><xsl:value-of select="format-number(area,'###,###.00', 'd')"/> km²</p>
		</div>
	</div>
	<hr></hr>
	<div class="row">
		<iframe
		  width="100%"
		  height="400"
		  frameborder="0"
		  marginheight="0"
		  marginwidth="0"
		  src="https://maps.google.com/maps?q={name}&amp;hl=en&amp;output=embed">
		</iframe>
	</div>
</xsl:template>

</xsl:stylesheet>
