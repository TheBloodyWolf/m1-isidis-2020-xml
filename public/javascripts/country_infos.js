async function display() {
    // TODO: load xml and xsl

    const xml = await get('../xml/countries.xml');
    const xsl = await get('../xsl/country_infos.xsl');

    const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
    // TODO: xsl transformations
    const proc = new XSLTProcessor();
 	proc.importStylesheet(xsl);
 	proc.setParameter(null, 'country_name', urlParams.get('country'));
 	const result = proc.transformToFragment(xml, document);
    // TODO: append in #frag_country_list
    let frag = document.querySelector('#frag_country_infos');
    frag.innerHTML='';
    frag.appendChild(result);
    hello();
}

async function hello(){
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	const country = urlParams.get('country');
	const xml = await get('../xml/countries.xml');
	let req = '//country[name="'+country+'"]/name/@cc';
	var cc = xml.evaluate(req, xml, null, XPathResult.STRING_TYPE, null ).stringValue;
	const hello = await get("https://fourtonfish.com/hellosalut/?cc="+cc, mode='json');
	let frag = document.querySelector('#say_hello');
	frag.innerHTML='';
	frag.innerHTML = JSON.parse(hello).hello;
}

display();
