const ARGS = { country_name: '', continent: ''};

async function display() {
    // TODO: load xml and xsl
    const { country_name, continent } = ARGS;

    const xml = await get('../xml/countries.xml');
    const xsl = await get('../xsl/country_list.xsl');

    // TODO: xsl transformations
    const proc = new XSLTProcessor();
 	proc.importStylesheet(xsl);
 	proc.setParameter(null, 'country_name', country_name);
 	proc.setParameter(null, 'continent', continent);
 	const result = proc.transformToFragment(xml, document);
    // TODO: append in #frag_country_list
    let frag = document.querySelector('#frag_country_list');
    frag.innerHTML='';
    frag.appendChild(result);
}

document.querySelector('#search_name').onkeyup = function() {
    ARGS.country_name = this.value;
    display();
  };

document.querySelector('#search_continent').onchange = function() {
    ARGS.continent = this.value;
    display();
  };

display();
